﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneDecider : MonoBehaviour {

    public GameObject perfectCutscene;
    public GameObject imperfectCutscene;

	// Use this for initialization
	void Start () {

        if( GameManager.instance.maxScoreAchieved == 99 )
        {
            GameObject.Instantiate(perfectCutscene);
        }
        else
        {
            GameObject.Instantiate(imperfectCutscene); //For now there is no imperfect cutscene.
        }

        Destroy(gameObject);
	}
}
