﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]
public class TheEnd : MonoBehaviour {

    public Text[] endText;
    public float fade = 0f;
    public float fadeRate = 0.2f;
    public float endTimeout = 30f;
    
    private AudioSource endSource;
    private float initialVolume;


    void Awake()
    {
        transform.parent = GameObject.Find("Canvas").transform;
    }

	// Use this for initialization
	void Start ()
    {
        endSource = GetComponent<AudioSource>();
        initialVolume = endSource.volume;
        transform.localPosition = new Vector3(0f, 0f, 0f );

        SetFade();
    }
	
	// Update is called once per frame
	void Update () {
		

        if(endTimeout > 0f )
        {
            if (fade < 1.0f)
            {
                fade += Time.deltaTime * fadeRate;
                fade = Mathf.Min(1.0f, fade);

                SetFade();
            }

            endTimeout -= Time.deltaTime;
            if (endTimeout <= 0f)
            {
                endTimeout = 0f;
                GameManager.instance.blackout.FadeTo(1f);
            }
        }
        else
        {
            if (fade > 0.0f)
            {
                fade -= Time.deltaTime * fadeRate;
                fade = Mathf.Max(0.0f, fade);

                SetFade();

                if( fade <= 0f )
                {
                    Application.Quit();
                }
            }
        }

    }

    //Fade everything 
    public void SetFade()
    {
        foreach( Text txt in endText )
        {
            Color col = txt.color;
            col.a = fade;
            txt.color = col;
        }
        endSource.volume = initialVolume * fade;
    }
}
