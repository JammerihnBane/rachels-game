﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

    public float hoverSpeed;
    public float hoverHeight;

    private Vector3 initialPosition;

    // Use this for initialization
    void Start () {
        initialPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 bounceAmount = new Vector3();

        bounceAmount.y = hoverHeight * Mathf.Sin(hoverSpeed * Time.timeSinceLevelLoad);

        transform.position = initialPosition + bounceAmount;
    }
}
