﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candle : MonoBehaviour {

    public int index;

	// Use this for initialization
	void Start () {
		
	}

    public void OnTriggerEnter2D()
    {
        GameManager.instance.CollectCandle(index);

        Destroy(gameObject);
    }
}
