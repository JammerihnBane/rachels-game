﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    public float fadeRate = 0.2f;

    private AudioSource source;
    private float desiredVolume = 1.0f;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        desiredVolume = source.volume;
        name = "Music";
    }
	
	// Update is called once per frame
	void Update () {
		
        if( source.volume < desiredVolume )
        {
            source.volume += Time.deltaTime * fadeRate;
            if (source.volume > desiredVolume)
                source.volume = desiredVolume;
        }

        if( source.volume > desiredVolume )
        {
            source.volume -= Time.deltaTime * fadeRate;
            if (source.volume < desiredVolume)
                source.volume = desiredVolume;
        }
            
    }
    
    public void FadeTo( float _value )
    {
        desiredVolume = _value;
    }
}
