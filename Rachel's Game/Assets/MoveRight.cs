﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour {

    public float speed;
    public float randomBoost;

    // Use this for initialization
    void Start()
    {
        speed = Random.Range(speed, speed + randomBoost);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        pos.x += speed * Time.deltaTime;

        transform.position = pos;

        if (transform.position.x >= 3000f)
            Destroy(gameObject);
	}
}
