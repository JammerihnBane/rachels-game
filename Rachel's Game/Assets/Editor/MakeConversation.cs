﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeConversation : MonoBehaviour {

    [MenuItem("Rachel's Game/Create/Conversation")]
    public static void CreateConversation()
    {
        Conversation asset = ScriptableObject.CreateInstance<Conversation>();

        AssetDatabase.CreateAsset(asset, "Assets/TempConversation.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
