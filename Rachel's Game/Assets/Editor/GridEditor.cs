﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

[CustomEditor(typeof(Grid))]
public class GridEditor : Editor {

    Grid grid;

    private string roomName = "DefaultRoom";
    private int oldIndex;
    private bool loadOnce = false;
    private bool connectionStarted = false;

    GameObject connectionObject = null;

    void OnEnable()
    {
        oldIndex = 0;
        grid = (Grid)(target);
        grid.Setup();

        roomName = grid.GetRoomName();
    }

    //This adds a menu item called "create tile set" that executes this code.
    [MenuItem("Assets/Create/Tileset")]
    static void CreateTileSet()
    {
        var asset = ScriptableObject.CreateInstance<TileSet>();
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);

        //String sense checking stuff.
        if ( string.IsNullOrEmpty(path))
        {
            path = "Assets";
        }
        else if( Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(path), "");
        }
        else
        {
            path += "/"; //Trailing slash, hmmmm...
        }

        var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "TileSet.asset");
        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("UserScripts/FixHideFlags")]
    static void FixHideFlags()
    {
        string[] str = AssetDatabase.FindAssets("TileSet");
        if (str != null && str.Length > 0)
        {
            foreach (string asset in str)
            {
                string properAssetPath = AssetDatabase.GUIDToAssetPath(asset);
                if (properAssetPath.Contains(".asset"))
                {
                    Debug.Log("Asset: " + properAssetPath);
                    TileSet set = AssetDatabase.LoadAssetAtPath<TileSet>(properAssetPath);
                    if (set != null)
                    {
                        Debug.Log("HideFlags: " + set.hideFlags);
                        set.hideFlags = HideFlags.None;
                        Debug.Log("HideFlags: " + set.hideFlags);
                        EditorUtility.SetDirty(set);

                        AssetDatabase.SaveAssets();
                    
                    }
                    else
                    {
                        Debug.Log("Found asset but couldn't load it?");
                    }
                }
            }
            
        }
        else
        {
            Debug.Log("Could not find asset.");
        }
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();        //We could call this to display all of the variables and stuff attached to this object.

        grid.width  = createSlider("Grid Width", grid.width);
        grid.height = createSlider("Grid Height", grid.height);

        if( GUILayout.Button("Open Grid Window"))
        {
            GridWindow window = (GridWindow)EditorWindow.GetWindow(typeof(GridWindow));
            window.init();
        }

        //Tile prefab?
        EditorGUI.BeginChangeCheck();
        //Creates an "object field" that can select any object.
        var newTilePrefab = (Transform)EditorGUILayout.ObjectField("Tile Prefab", grid.tilePrefab, typeof(Transform), false);
        if(EditorGUI.EndChangeCheck())
        {
            grid.tilePrefab = newTilePrefab;
            //Lets the undo command know about this object.
            Undo.RecordObject(target, "Grid Changed");
        }

        //Tileset.
        EditorGUI.BeginChangeCheck();
        var newTileSet = (TileSet)EditorGUILayout.ObjectField("Tileset", grid.tileSet, typeof(TileSet), false);
        if (EditorGUI.EndChangeCheck())
        {
            grid.tileSet = newTileSet;
            Undo.RecordObject(target, "TileSet Changed");
        }

        if (grid.tileSet != null)
        {
            EditorGUI.BeginChangeCheck();
            var names = new string[grid.tileSet.prefabs.Length];
            var values = new int[names.Length];

            for (int i = 0; i < names.Length; ++i)
            {
                names[i] = grid.tileSet.prefabs[i] != null ? grid.tileSet.prefabs[i].name : "";
                values[i] = i;
            }

            //Selet a tile to paint on the map.
            var index = EditorGUILayout.IntPopup("Select Tile", oldIndex, names, values);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Grid Changed");
                oldIndex = index;
                grid.tilePrefab = grid.tileSet.prefabs[index]; //Change selected tile.
            }
        }

        //Create a whole bunch of buttons and toggles and whatnot.
        CreateSaveLoad();
        CreateSetActiveRoom();
        CreateShowVisualisations();

        Event e = Event.current;
        if (e.commandName == "ObjectSelectorClosed")
        {
            e.Use();

            if( loadOnce == true )
            {
                loadOnce = false;
                grid.loadedData = EditorGUIUtility.GetObjectPickerObject() as RoomData;
                if(grid.loadedData != null )
                {
                    grid.ClearObjects();

                    //Debug.Log("An object was picked! This one!: " + grid.loadedData.ToString());
                    SaveLoader.LoadRoomData(grid.loadedData, grid.tilesHolder.transform, grid.objectsHolder.transform);

                    string path = AssetDatabase.GetAssetPath(grid.loadedData);
                    grid.SetupRoomPath( path );
                    roomName = grid.GetRoomName();
                    //Debug.Log("Room name: " + grid.GetRoomName());
                }
            }
         }
    }

    public void OnSceneGUI()
    {
        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        Event e = Event.current;
        Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
        Vector3 mousePos = ray.origin;

        //---------------------------------------------------------------------------------------------------------------
        //MOUSE STUFF!!
        //Check if the mouse button is clicked.
        if (e.isMouse == true && e.type == EventType.MouseDown)
        {
            
            Vector3 alignedPos = new Vector3(Mathf.Floor(mousePos.x / grid.width) * grid.width + grid.width * 0.5f,
                                               Mathf.Floor(mousePos.y / grid.height) * grid.height + grid.height * 0.5f,
                                               0.0f);

            if (e.button == 0)
            {
                GameObject gameObject;
                Transform prefab = grid.tilePrefab;

                if (prefab)
                {
                    //Create and position the currently selected tile.
                    gameObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab.gameObject);

                    gameObject.transform.position = alignedPos;
                    if (gameObject.GetComponent<TileData>())
                        gameObject.transform.parent = grid.tilesHolder.transform;
                    else
                        gameObject.transform.parent = grid.objectsHolder.transform;

                    SaveLoader.TriggerObjectScripts(gameObject);

                    Undo.IncrementCurrentGroup();
                    Undo.RegisterCreatedObjectUndo(gameObject, "Create" + gameObject.name);
                }

                GUIUtility.hotControl = controlID;
            }
            else if (e.button == 1) //Delete thing at position when right clicked.
            {
                Transform selected = MordEditorUtilities.GetTransformFromPosition(alignedPos, grid.transform);
                if (selected != null)
                {
                    Undo.DestroyObjectImmediate(selected.gameObject);
                    Debug.Log("Destroying this thing...");
                }
            }
            else if(e.button == 2)
            {
                Debug.Log("Test");
                ProcessConnectingObjects(alignedPos);
            }
        }

        if (e.isMouse == true && e.type == EventType.MouseUp && e.button == 0 )
        {
           GUIUtility.hotControl = 0;
        }
        //---------------------------------------------------------------------------------------------------------------
    }

    //This code is what lets two objects be connected together in the editor.
    private void ProcessConnectingObjects( Vector3 _alignedPos)
    {
        Debug.Log("Checking for an object connections...");
        if (connectionStarted == false)
        {
            Transform connectionTransform = MordEditorUtilities.GetTransformWithExtraData(_alignedPos, grid.transform);
            if (connectionTransform != null)
            {
                connectionObject = connectionTransform.gameObject;
                connectionStarted = true;
                Debug.Log("Connection Started.");
                grid.connectionObject = connectionObject;
            }
        }
        else
        {
            Transform endConnector = MordEditorUtilities.GetTransformWithExtraData(_alignedPos, grid.transform);
            if (endConnector != null && connectionObject != endConnector)
            {
                ExtraData endED = endConnector.GetComponent<ExtraData>();
                if ( endED != null )
                {
                    if (endED.rawData.objectTag == "")
                        endED.rawData.objectTag = MordEditorUtilities.GetUniqueName(endConnector.gameObject);

                    connectionObject.GetComponent<ExtraData>().AddConnection(endED.rawData.objectTag);
                }
            }

            //Reset variables.
            Debug.Log("Connection Ended.");
            connectionStarted = false;
            connectionObject = null;
            grid.connectionObject = null;
        }
    }

    //Creates a slider.
    private float createSlider(string labelName, float sliderPosition)
    {
        GUILayout.BeginHorizontal();

        GUILayout.Label(labelName);
        sliderPosition = EditorGUILayout.Slider(sliderPosition, 1f, 100f, null);
        GUILayout.EndHorizontal();

        return sliderPosition;
    }


    //Creates the "save room" and "load room buttons, along with a text field for the room name.
    private void CreateSaveLoad()
    {
        GUILayout.BeginHorizontal();

            GUILayout.Label("Room Name");
            roomName = GUILayout.TextField(roomName);

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        //"Save Room" Button.
        if (GUILayout.Button("Save Room"))
        {
            if (roomName == "")
                roomName = "DefaultRoom";

            RoomData data = SaveLoader.GenerateRoomDataFromTransform(grid.tilesHolder.transform, grid.objectsHolder.transform, grid.tileSet);
            data.tileSet = grid.tileSet;
            if (roomName.Contains(".asset") == false)
                roomName += ".asset";

            string path = grid.GetRoomPath();
            Debug.Log("Path: " + path);
            if( path != "" && path != Grid.DEFAULT_PATH )
                AssetDatabase.CreateAsset(data, path);
            else
            {
                AssetDatabase.CreateAsset(data, "Assets/Resources/Rooms/" + roomName);
            }

            RoomData newRoom = (RoomData)(AssetDatabase.LoadAssetAtPath(path, typeof(RoomData)));
            newRoom.name = ""; //Just doing this to avoid a warning.
            newRoom = data;

            AssetDatabase.SaveAssets();
        }
        if( GUILayout.Button("Load Room") )
        {
            loadOnce = true;
            grid.Reset();
            EditorGUIUtility.ShowObjectPicker<RoomData>(null, false, "", 0);
        }

        GUILayout.EndHorizontal();

        //Big ol' "Clear objects button.
        GUILayout.BeginHorizontal();

        if( GUILayout.Button("Clear Objects") )
        {
            if (EditorUtility.DisplayDialog("Clear Objects?", "Are you sure you want to clear everything?", "Clear", "Cancel"))
            {
                grid.ClearObjects();
                grid.Reset();
                roomName = "DefaultRoom";
            }
        }

        GUILayout.EndHorizontal();
    }

    void CreateSetActiveRoom()
    {
        GUILayout.BeginHorizontal();

        if( GUILayout.Button("Set As Starting Room") )
        {
            GameObject obj = GameObject.Find("WorldManager");
            WorldManager wm = obj.GetComponent<WorldManager>();
            if (wm != null)
                wm.initialRoom = roomName;
        }

        GUILayout.EndHorizontal();
    }

    //Creates a toggle to show/hide visualisations.
    void CreateShowVisualisations()
    {
        GUILayout.BeginHorizontal();
        ExtraData.visualisations = EditorGUILayout.Toggle("Visual Connections: ", ExtraData.visualisations, GUILayout.ExpandWidth(true));
            
        GUILayout.EndHorizontal();
    }
}
