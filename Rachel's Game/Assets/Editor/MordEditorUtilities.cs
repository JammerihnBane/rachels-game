﻿using UnityEngine;
using System.Collections;

public class MordEditorUtilities : MonoBehaviour {

    public static Transform GetTransformFromPosition(Vector3 _pos, Transform _topLevel)
    {
        int i = 0;
        while (i < _topLevel.childCount)
        {
            Transform child = _topLevel.GetChild(i);

            //Recursively iterate through child transforms.
            if (child.childCount > 0)
            {
                Transform t = GetTransformFromPosition(_pos, child.transform);
                if (t != null)
                {
                    return t;
                }
            }

            //Child transform didn't have anything there, so we'll check whatever is in the top level.
            Vector3 dist = _pos - child.transform.position;
            dist.z = 0.0f;
            if (dist.sqrMagnitude < 0.2f)
            {
                return child;
            }
            ++i;
        }
        return null;
    }

    public static Transform GetTransformWithExtraData(Vector3 _pos, Transform _topLevel)
    {
        int i = 0;
        while (i < _topLevel.childCount)
        {
            Transform child = _topLevel.GetChild(i);

            //Recursively iterate through child transforms.
            if (child.childCount > 0)
            {
                Transform t = GetTransformWithExtraData(_pos, child.transform);
                if (t != null)
                {
                    return t;
                }
            }

            //Child transform didn't have anything there, so we'll check whatever is in the top level.
            Vector3 dist = _pos - child.transform.position;
            dist.z = 0.0f;
            if (dist.sqrMagnitude < 0.2f && child.GetComponent<ExtraData>() != null )
            {
                return child;
            }
            ++i;
        }
        return null;
    }

    //Tries to generate a unique name for this object, based on how many objects of its type exist.
    //This only checks objects with an "ExtraData" component.
    public static string GetUniqueName(GameObject _obj, GameObject _objHolder = null )
    {
        string name = "";
        if (_obj)
        {
            //Find the parent object of this thing so we know what to compare it to.
            GameObject objectHolder = null;
            if (_objHolder == null)
            {
                objectHolder = GameObject.Find("objectsHolder");
            }
            else
                objectHolder = _objHolder;
            
            //Count up how many objects of the same type exist.
            //Starting point for our name.
            int numObjects = 0;
            foreach (Transform trans in objectHolder.transform)
            {
                if( trans.name == _obj.name )
                {
                    ++numObjects;
                }
            }
            
            //Our name starts out as "objectNameX" where X is the number of objects with a similar name.
            //X increases until we find an object that doesn't have the same tag as us.
            while( true )
            {
                string testName = _obj.name.ToLower() + (numObjects + 1).ToString();
                bool duplicateName = false;
                foreach( Transform trans in objectHolder.transform)
                {
                    ExtraData ed = trans.GetComponent<ExtraData>();
                    if (ed != null && ed.rawData.objectTag == testName)
                    {
                        duplicateName = true;
                        break;
                    }
                }

                if (duplicateName == false)
                {
                    name = testName;
                    break;
                }
                else
                {
                    ++numObjects;
                }
            }

        }
        else
        {
            Debug.Log("Can't generate a unique name for a thing that doesn't exist.");
        }
        return name;
    }
}
