﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FloatyHeart : MonoBehaviour {

    public float ySpeed = 3f;
    public float fade = 0.2f;
    public float hoverAmp = 4;
    public float hoverSpeed = 1f;

    private SpriteRenderer rend;
    private Vector3 initialPosition;

	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteRenderer>();
        initialPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;

        pos.y += ySpeed * Time.deltaTime;

        pos.x = initialPosition.x + hoverAmp * Mathf.Sin(hoverSpeed * Time.timeSinceLevelLoad);

        transform.position = pos;

        Color col = rend.color;

        //Fade out the thing.
        col.a -= fade * Time.deltaTime;
        if (col.a <= 0f)
            Destroy(gameObject);
        else
            rend.color = col;
	}
}
