﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingNovanator : MonoBehaviour {

    public float upwardsForce;

    private Vector3 velocity = Vector3.zero;
    private Vector3 accel = Vector3.zero;
    private AudioSource audio;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        velocity.y += upwardsForce * Time.deltaTime;
        accel += velocity * Time.deltaTime;

        transform.position += accel;

        if (audio.isPlaying == false)
            Destroy(gameObject);
    }
}
