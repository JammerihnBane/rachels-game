﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomData : ScriptableObject{

    public int[] mapTiles;
    public TileSet tileSet;
    public int mapWidth = 0;
    public int mapHeight = 0;
    
    public int[] mapObjects;
    public int[] mapObjectPositions;

    //Extra data, hooray!
    public List<int>        extraDataIDs;
    public List<RawExtraData>    extraData;

    //Creates the tile array for the room data.
    //DOES NOT fill up the array.
    public void SetupTileArray( int _width, int _height )
    {
        mapWidth = _width;
        mapHeight = _height;
        mapTiles = new int[_width *_height];
        for (int i = 0; i < _width * _height; ++i)
            mapTiles[i] = -1;
    }

    public int PositionToID( Vector3 _position )
    {
        int id = -1;

        id = Mathf.RoundToInt( _position.x + _position.y * mapWidth );

        return id;
    }

    public void IDToPosition( int id, out float _x, out float _y )
    {
        _x = id % mapWidth;
        _y = id / mapWidth;
    }

    public int PositionToID( int _x, int _y)
    {
        return (_y * mapWidth + _x);
    }

    //Object ID is provided from the tileset.
    public void SetTile(int _x, int _y, int _objectID)
    {
        int id = (_x + _y * mapWidth);
        if (id >= 0 && id < mapTiles.Length)
        {
            mapTiles[id] = _objectID;
        }
    }

    public int GetTile(int _x, int _y)
    {
        int id = (_x + _y * mapWidth);
        if (id >= 0 && id < mapTiles.Length)
        {
            return mapTiles[id];
        }
        return -1;
    }
}