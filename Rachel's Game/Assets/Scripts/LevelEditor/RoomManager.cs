﻿using UnityEngine;
using System.Collections;

public class RoomManager : MonoBehaviour {

    public static RoomManager instance = null;
    RoomData[] allRooms = null;

    // Use this for initialization
    void Awake ()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            allRooms = Resources.LoadAll<RoomData>("");
        }
        else
        {
            DestroyImmediate(gameObject);
        }
	}

    public RoomData LoadRoomData(string _name)
    {
        if( _name.Contains(".") )
        {
            _name = _name.Split('.')[0];
        }

        foreach( RoomData room in allRooms )
        {
            if (room.name == _name)
                return room;
        }

        return null;
    }
}
