﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldManager : MonoBehaviour {

    public static WorldManager worldManager = null;
    public string initialRoom;
    public GameObject clearAllFromThis; //The game will clear all objects from this object on startup.

    public List<Room> roomList = new List<Room>();

    //Clear everything from the scene before even starting.
    void Awake()
    {
        //SaveGameManager.Create();
        //SaveGameManager.instance.LoadDataFromDisk();

        if (clearAllFromThis != null)
        {
            Utilities.ClearAllFromThis(clearAllFromThis);
        }
    }

	// Use this for initialization
	void Start () {

        if( worldManager == null )
        {
            worldManager = this;
            if (initialRoom == null)
                Debug.LogError("There's no starting room. Uh oh.");
            LoadFloor(initialRoom);
        }
        else
        {
            Debug.LogError("TWO World Managers have been created. What's happening?!");
        }
	}

    public void AddRoom(Room _room)
    {
        roomList.Add(_room);
    }

    //Loads every room connected to the starting room, and the starting room too.
    //Returns the starting room.
    public Room LoadFloor( string _startingRoom )
    {
        Room startingRoom = LoadRoom(_startingRoom);
        if (startingRoom != null )
        {
            LoadFloor(startingRoom);
        }
        else
        {
            Debug.LogError("Couldn't load room: " + _startingRoom);
        }
        return startingRoom;  
    }

    //Loads every room connected to the starting room, and the starting room too.
    //Returns the starting room.
    public Room LoadFloor( RoomData _startingRoom )
    {
        Room startingRoom = LoadRoom(_startingRoom);
        return LoadFloor(startingRoom);
    }

    //Common functionality for load floor.
    //This functionality relies on the first room already existing.
    private Room LoadFloor( Room _startingRoom )
    {
        Room currentRoom = _startingRoom;
        List<Room> nextRooms = new List<Room>();

        while (currentRoom != null)
        {
            foreach (ExtraData data in currentRoom.connections)
            {
                //Split the connection string. Strings are in the format "Room.objectTag"
                if (data.rawData.connections.Length > 0)
                {
                    string[] connectionTo = data.rawData.connections[0].Split('.');
                    if (RoomExists(connectionTo[0]) == false)
                    {
                        Room otherRoom = LoadRoom(connectionTo[0]);
                        nextRooms.Add(otherRoom);
                        Room.ConnectRooms(currentRoom, otherRoom, data.rawData.objectTag, connectionTo[1]);
                    }
                }
            }

            //Update the current room.
            if (nextRooms.Count > 0)
            {
                currentRoom = nextRooms[0];
                nextRooms.RemoveAt(0);
            }
            else
            {
                //Break condition.
                currentRoom = null;
            }
        }
        return _startingRoom;
    }

    //If the room exists and has been loaded into the scene, this returns true.
    public bool RoomExists( string _roomName )
    {
        foreach (Room room in roomList)
        {
            if( room.Name() == _roomName )
            {
                return true;
            }
        }
        return false;
    }

    //Loads an individual room into a "room" object.
    public Room LoadRoom( string _path )
    {
        Room room = new Room();
        if (room.Initialise(_path) == true)
        {
            AddRoom(room);
        }
        else
            room = null;

        return room;
    }


    //Loads an individual room into a "room" object.
    public Room LoadRoom(RoomData _room)
    {
        Room room = new Room();
        if (room.Initialise(_room) == true)
        {
            AddRoom(room);
        }
        else
            room = null;

        return room;
    }

    //Returns the current room.
    public Room CurrentRoom()
    {
        //return GetRoom(Player.player.transform.position);
        return null;
    }

    public Room GetRoom( Vector3 _position )
    {
        foreach( Room room in roomList )
        {
            if( room.IsWithinRoom( _position ) )
            {
                return room;
            }
        }
        return null;
    }
    
    //Returns the tile at the specified world position.
    public GameObject GetTile( Vector3 _position )
    {
        foreach(Room room in roomList )
        {
            if( room.IsWithinRoom( _position ))
            {
                return room.GetTileAtWorldPosition(_position);
            }
        }
        return null;
    }

    //Returns all of the objects at a specific position.
    public List<GameObject> GetObjects( Vector3 _position )
    {
        List<GameObject> objects = new List<GameObject>();
        foreach (Room room in roomList)
        {
            if (room.IsWithinRoom(_position))
            {
                room.GetObjectsAtWorldPosition(_position, objects);
            }
        }
        return objects;
    }

    //Activates all objects at the position specified.
    //public void ActivateObjectsAt( Vector3 _position, GameObject _activator )
    //{
    //    List<GameObject> objects = GetObjects(_position);
    //    if( objects != null && objects.Count > 0 )
    //    {
    //        foreach( GameObject obj in objects )
    //        {
    //            Activator act = obj.GetComponent<Activator>();
    //            if( act != null )
    //            {
    //                act.Activate(_activator);
    //            }
    //        }
    //    }
    //}

    //public void DeactivateObjectsAt(Vector3 _position, GameObject _activator)
    //{
    //    List<GameObject> objects = GetObjects(_position);
    //    if (objects != null && objects.Count > 0)
    //    {
    //        foreach (GameObject obj in objects)
    //        {
    //            Activator act = obj.GetComponent<Activator>();
    //            if (act != null)
    //            {
    //                act.Deactivate(_activator);
    //            }
    //        }
    //    }
    //}

    //Will return the named object specified. Only works within the same room
    //as the position specified.
    public GameObject GetNamedObject( string _name, Vector3 _currentPosition )
    {
        foreach( Room _room in roomList )
        {
            if(_room.IsWithinRoom(_currentPosition))
            {
                return _room.GetNamedObject(_name);
            }
        }
        return null;
    }

    //This will teleport the player to a specific object with the given tag.
    //The roomConnection string should be in the format "Room.objectTag", as
    //the function will split the connection string using '.' as a delimiter.
    public void TeleportPlayer( string _roomConnection )
    {
        string[] roomAddress = _roomConnection.Split('.');
        if (roomAddress.Length <= 1)
            Debug.Log("This address doesn't contain a '.' ");

        if( RoomExists( roomAddress[0] ) == false )
        {
            DestroyAllRooms();
            Room newRoom = LoadFloor(roomAddress[0]);
            GameObject obj = newRoom.GetNamedObject(roomAddress[1]);
            if( obj != null )
            {
                Vector3 newPos = obj.transform.position;
                //newPos.z = Player.player.transform.position.z;
                //Player.player.transform.position = newPos;
            }
            else
            {
                Debug.LogError("Can't teleport the player, the object named: \"" + roomAddress[1] + "\" does not exist.");
            }
        }
    }

    //Destroys every room on the floor and removes it from the room list.
    public void DestroyAllRooms()
    {
        while( roomList.Count > 0 )
        {
            Room toDestroy = roomList[0];
            roomList.RemoveAt(0);

            toDestroy.DestroyRoom();
        }
    }

    //Returns whether the selected position in world space is free.
    public bool IsPositionFree( Vector3 _position, bool _bCaresAboutSemiSolid = false )
    {
        bool isFree = true;

        GameObject tile = GetTile(_position);
        List<GameObject> objects = GetObjects(_position);

        TileData td = null;
        if (tile != null)
            td = tile.GetComponent<TileData>();

        //foreach( GameObject obj in objects)
        //{
        //    SolidActiveObject sao = obj.GetComponent<SolidActiveObject>();
        //    if( sao != null )
        //    {
        //        if( sao.solid == true )
        //            isFree = false;
        //        if (sao.semiSolid == true && _bCaresAboutSemiSolid == true)
        //            isFree = false;
        //    }
        //}
        
        if (td != null && td.solid == true)
            isFree = false;

        return isFree;
    }
}
