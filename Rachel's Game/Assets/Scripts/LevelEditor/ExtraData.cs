﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RawExtraData
{
    public string[] connections;
    public string   objectTag;
    public bool     state;

    public void Set( ExtraData _data )
    {
        objectTag = _data.rawData.objectTag;
        connections = _data.rawData.connections;
        state = _data.rawData.state;
    }

    public ExtraData Get()
    {
        ExtraData ed = new ExtraData();
        ed.rawData = this;
        return ed;
    }
}

[System.Serializable]
public class ExtraData : MonoBehaviour
{
    //This class will be used by the Level Editor.
    //Extra data classes are saved seperately and
    //attached to their requisite classes when the
    //level is built.

    //ONLY USEFUL FOR "STATIC" DATA
    public RawExtraData rawData;

    private GameObject oh = null;
    public static bool visualisations = true;

    public void AddConnection( string _connectTo )
    {
        if(_connectTo != "" )
        {
            string[] newConnections = new string[rawData.connections.Length + 1];
            for (int i = 0; i < rawData.connections.Length; ++i)
                newConnections[i] = rawData.connections[i];

            newConnections[newConnections.Length - 1] = _connectTo;

            rawData.connections = newConnections;
        }
        else
        {
            Debug.LogError("Can't add a connection with an empty connection string.");
        }
    }

    //This will draw lines to any connected objects in scene view.
    public void OnDrawGizmos()
    {
        //If the visualisations are enabled, try to draw a line between our object and our connections.
        if (visualisations == true)
        {
            //Debug.Log("Drawing Gizmos");
            if (oh == null)
                oh = GameObject.Find("objectsHolder"); //Grab object holder. Hideously slow but whatevs.

            if (rawData.connections.Length > 0 && oh != null)
            {
                //Debug.Log("Got past step 1.");
                foreach (string connection in rawData.connections)
                {
                    ExtraData ed = null;
                    foreach (Transform child in oh.transform)
                    {
                        ed = child.GetComponent<ExtraData>();
                        if (ed != null && ed.rawData.objectTag == connection)
                        {
                            //Debug.Log("found object");
                            break;
                        }
                        else
                        {
                            ed = null;
                        }
                    }
                    //If the step above fills out the extradata variable, it will
                    //let us draw a line between this object and the object it found.
                    if (ed != null)
                    {
                        //Debug.Log("Drawing line.");
                        Gizmos.color = Color.green;
                        Gizmos.DrawLine(transform.position, ed.transform.position);
                    }
                }
            }
        }
    }
}
