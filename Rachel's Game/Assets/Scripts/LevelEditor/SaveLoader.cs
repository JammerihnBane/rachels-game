﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public class SaveLoader : MonoBehaviour {

    //Creates a RoomData ScriptableObject that contains all of the information about the room.
    static public RoomData GenerateRoomDataFromTransform(Transform _tileData, Transform _objectData, TileSet _tileSet)
    {
        RoomData data = ScriptableObject.CreateInstance<RoomData>();
        data.extraDataIDs   = new List<int>();
        data.extraData      = new List<RawExtraData>();

        //Find the bottom-left edge and top-right edge of the map.
        Vector3 bottomLeft  = new Vector3(99999.0f, 99999.0f, 0.0f);
        Vector3 topRight    = new Vector3(-99999.0f, -99999.0f, 0.0f);
        foreach (Transform tile in _tileData)
        {
            //Update the bottom-left corner of the map.
            if( tile.transform.position.x < bottomLeft.x )
                bottomLeft.x = tile.transform.position.x;
            if (tile.transform.position.y < bottomLeft.y)
                bottomLeft.y = tile.transform.position.y;
            //Update the top-right coordinate of the map.
            if (tile.transform.position.x > topRight.x)
                topRight.x = tile.transform.position.x;
            if (tile.transform.position.y > topRight.y)
                topRight.y = tile.transform.position.y;
        }
         
        //Debug.Log( "Top Right: " + topRight.ToString() );
        Vector3 widthHeight = topRight - bottomLeft;
        int width   = Mathf.RoundToInt( Mathf.Abs(widthHeight.x) + 1 );
        int height  = Mathf.RoundToInt( Mathf.Abs(widthHeight.y) + 1 );

        data.SetupTileArray( width, height );

        //Save each individual tile in the map.
        foreach (Transform tile in _tileData)
        {
            int arrayX = Mathf.RoundToInt((tile.position - bottomLeft).x); //If we have tiles from 0-8, we'll have to have a 9 tile-wide array. Hence +1.
            int arrayY = Mathf.RoundToInt((tile.position - bottomLeft).y); //If we have tiles from 0-8, we'll have to have a 9 tile-high array. Hence +1.

            //Debug.Log("tile.Position: " + tile.position + " bottomLeft: " + bottomLeft + " array coords: (" + arrayX + ", " + arrayY + ")" );

            data.SetTile(arrayX, arrayY, _tileSet.ToID( tile.gameObject ) );
        }

        SetupMapObjects(_objectData, _tileSet, data, bottomLeft);

        return data;
    }

    //Saves all the objects on the map to the array.
    static private void SetupMapObjects(Transform _objectData, TileSet _tileSet, RoomData _data, Vector3 _posShift)
    {
        int numChildren = _objectData.childCount;
        if (numChildren > 0)
        {
            _data.mapObjects            = new int[numChildren];
            _data.mapObjectPositions    = new int[numChildren];

            int index = 0;
            //For each object on the map.
            foreach (Transform child in _objectData)
            {
                //Find out where it exists in the tileset.
                int id = _tileSet.ToID(child.gameObject);
                if (id != -1)
                {
                    _data.mapObjects[index]         = id;
                    _data.mapObjectPositions[index] = _data.PositionToID(child.position - _posShift);

                    //Get any extradata component the object might have.
                    ExtraData extraData = child.GetComponent<ExtraData>();
                    if( extraData != null )
                    {
                        RawExtraData rData = new RawExtraData();
                        rData.Set(extraData);

                        _data.extraDataIDs.Add(index);
                        _data.extraData.Add(rData);
                    }
                }
                else
                    Debug.LogError("A game object in this room does not fit the tileSet.");

                ++index;
            }
        }
    }

    static public RoomData LoadRoomData( string _name, Transform _tileHolder, Transform _objectsHolder )
    {
        //Third attempt at loading data. This loads up every single room into memory though, so fuck.
        RoomData room = RoomManager.instance.LoadRoomData(_name);
        if ( room != null )
            LoadRoomData(room, _tileHolder, _objectsHolder);
        return room;
    }

    static public void LoadRoomData( RoomData _room, Transform _tileHolder, Transform _objectsHolder )
    {
        LoadTiles( _room, _tileHolder );
        LoadObjects( _room, _objectsHolder );
    }

    static private void LoadTiles( RoomData _room, Transform _tileHolder )
    {
        //Debug.Log("LoadRoomDataCalled");
        for (int y = 0; y < _room.mapHeight; ++y)
        {
            for (int x = 0; x < _room.mapWidth; ++x)
            {
                int tileID = _room.GetTile(x, y);
                if (tileID != -1)
                {
                    Transform trans = _room.tileSet.prefabs[tileID];
                    if (trans != null)
                    {
                        GameObject obj = MakeGameObject(trans.gameObject);

                        obj.transform.position = new Vector3((float)(x) + 0.5f, (float)(y) + 0.5f);
                        obj.transform.parent = _tileHolder;

                        TriggerObjectScripts(obj);
                    }
                }
            }
        }
    }

    static public void LoadObjects(RoomData _room, Transform _objectsHolder, bool _loadImmidiately = true)
    {
        for (int i = 0; i < _room.mapObjects.Length; ++i)
        {
            Transform trans = _room.tileSet.prefabs[_room.mapObjects[i]];
            if (trans != null)
            {
                //Creates the game object.
                GameObject obj = MakeGameObject(trans.gameObject);
                //Debug.Log("Instatiated a: " + obj);
                

                Vector3 pos = Vector3.zero;
                _room.IDToPosition(_room.mapObjectPositions[i], out pos.x, out pos.y);
                //Move object into the middle of the tile.
                pos.x += 0.5f; pos.y += 0.5f;

                obj.transform.position = pos;
                obj.transform.parent = _objectsHolder;

                //This will overwrite the position given by the room.
                //SetupSaveData(obj, i, _room.name, _loadImmidiately);

                TriggerObjectScripts(obj);
            }
        }

        LoadExtraData(_room, _objectsHolder);
    }

    static private GameObject MakeGameObject( GameObject _original )
    {
#if UNITY_EDITOR
        return (GameObject)PrefabUtility.InstantiatePrefab(_original.gameObject);
#else
        return (GameObject)GameObject.Instantiate(_original.gameObject);
#endif
    }

    //Uses the object's position in the array (bad) to determine the extra data for it to load.
    static private void LoadExtraData( RoomData _room, Transform _objectsHolder)
    {
        for( int i = 0; i < _room.extraData.Count; ++i )
        {
            RawExtraData data   = _room.extraData[i];
            int ID              = _room.extraDataIDs[i];

            if( data != null )
            {
                ExtraData currentData = _objectsHolder.GetChild(ID).gameObject.GetComponent<ExtraData>();
                currentData.rawData = data;
                //Debug.Log("Object Tag: " + currentData.rawData.objectTag);
            }
        }
    }

    static public void TriggerObjectScripts(GameObject _obj)
    {
        ForceDepth fd = _obj.GetComponent<ForceDepth>();
        if (fd != null)
            fd.DepthUpdate();
    }

    //static private void SetupSaveData(GameObject _obj, int _objectID, string _roomName, bool _loadImmidiately )
    //{
    //    if (_obj)
    //    {
    //        SaveData dat = _obj.GetComponent<SaveData>();
    //        if (dat != null)
    //        {
    //            dat.SetObjectID(_roomName, _objectID, _loadImmidiately);
    //        }
    //    }
    //    else
    //    {
    //        Debug.LogError("Can't setup save data on a null object.");
    //    }
    //}
}
