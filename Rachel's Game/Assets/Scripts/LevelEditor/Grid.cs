﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

    public static string DEFAULT_PATH = "DEFAULT_ROOM_PATH";

    public float width = 32.0f;
    public float height = 32.0f;

    public Color color = Color.green;

    public Transform tilePrefab;
    public TileSet tileSet;

    public GameObject tilesHolder       = null;
    public GameObject objectsHolder     = null;
    public GameObject roomPathObject    = null;

    public RoomData loadedData = null;
    public string loadedName = "";

    public GameObject connectionObject; //If selected, the game will attempt to draw a line between the mouse position and this object.
    
    public void Reset()
    {
        loadedData = null;
        if( roomPathObject != null )
        {
            roomPathObject.name = DEFAULT_PATH;
        }
        loadedName = "";
    }

    public void Setup()
    {
        if (tilesHolder == null)
        {
            tilesHolder = new GameObject("tileHolder");
            tilesHolder.transform.parent = transform;
        }

        if (objectsHolder == null)
        {
            objectsHolder = new GameObject("objectsHolder");
            objectsHolder.transform.parent = transform;
        }

        if( roomPathObject == null )
        {
            roomPathObject = new GameObject("roomPathHolder");
            roomPathObject.transform.parent = transform;
        }
    }

    public void SetupRoomPath(string _strPath)
    {
        roomPathObject.name = _strPath;
    }

    public string GetRoomPath()
    {
        if (roomPathObject != null && roomPathObject.name != DEFAULT_PATH)
        {
            return roomPathObject.name;
        }
        else
        {
            return "";
        }
    }

    public string GetRoomName()
    {
        return loadedName;
    }

    public void OnDrawGizmos()
    {
        if( width >= 1.0f && height >= 1.0f )
        {
            Vector3 pos = Camera.current.transform.position;
            Gizmos.color = color;

            ////Draw vertical lines.
            //for (float y = pos.y - 800.0f; y < pos.y + 800.0f; y += this.height)
            //{
            //    Gizmos.DrawLine(new Vector3(-1000000f, Mathf.Floor(y / height) * height, 0.0f), new Vector3(1000000f, Mathf.Floor(y / height) * height, 0.0f));
            //}

            ////Draw vertical lines.
            //for (float x = pos.x - 800.0f; x < pos.x + 800.0f; x += this.width)
            //{
            //    Gizmos.DrawLine(new Vector3(Mathf.Floor(x / width) * width, -1000000f, 0.0f), new Vector3(Mathf.Floor(x / width) * width, 1000000f, 0.0f));
            //}
        }

        if (connectionObject != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(connectionObject.transform.position, 0.3f);
            //Ray ray = Camera.current.ScreenPointToRay(Input.mousePosition);
            
            //Vector3 mousePoint = 
            //Gizmos.DrawLine(connectionObject.transform.position, mousePoint);
            Debug.Log("Drawing Line");
        }
    }

    public void ClearObjects()
    {
        ClearAllFrom(tilesHolder.transform);
        ClearAllFrom(objectsHolder.transform);
    }

    //Clears all gameobjects from a transform, and all gameobjects from that transform's children.
    private void ClearAllFrom( Transform _trans )
    {
        while( _trans.childCount > 0 )
        {
            Transform child = _trans.GetChild(0);
            if (child.childCount > 0)
                ClearAllFrom(child);

            GameObject.DestroyImmediate(child.gameObject);
        }
    }
}
