﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public class Room {

    GameObject roomHolder;
    GameObject tileHolder;
    public GameObject objectHolder;
    RoomData data;

    private GameObject[] tiles = null; 

    public List<ExtraData> connections = new List<ExtraData>();

    public bool Initialise(string _assetName)
    {
        bool bSuccess = true;

        SetupTransforms();

        data = SaveLoader.LoadRoomData(_assetName, tileHolder.transform, objectHolder.transform);
        if (data != null)
        {
            CommonInitialise();
        }
        else
        {
            bSuccess = false;
            Debug.LogError("cannot find room: " + _assetName);
        }

        return bSuccess;
    }

    public bool Initialise(RoomData _data )
    {
        data = _data;
        SetupTransforms();
        SaveLoader.LoadRoomData(data, tileHolder.transform, objectHolder.transform);
        CommonInitialise();

        return true;
    }

    public void CommonInitialise()
    {
        LoadTiles();
        roomHolder.name = data.name;

        LoadConnections();
    }

    //Orders the tiles into an array for easy retrieval.
    private void LoadTiles()
    {
        tiles = new GameObject[data.mapWidth * data.mapHeight];
        foreach (Transform _child in tileHolder.transform)
        {
            int index = ToIndex(_child.position - roomHolder.transform.position);

            tiles[index] = _child.gameObject;
        }
    }

    public int WorldPosToIndex( Vector3 _transformPos )
    {
        Vector3 localPos = _transformPos - roomHolder.transform.position;
        return ToIndex(localPos);
    }

    //Turns a local position into an index.
    private int ToIndex( Vector3 _localPos )
    {
        return Mathf.RoundToInt( (Mathf.Floor(_localPos.y) * data.mapWidth) + Mathf.Floor(_localPos.x));
    }

    //Sets up a bunch of empty transforms for the room.
    void SetupTransforms()
    {
        roomHolder = new GameObject();
        roomHolder.name = "TempRoomName";

        tileHolder = new GameObject();
        tileHolder.name = "TileHolder";

        NewObjectHolder();

        //Set these empty game objects as children of the room.
        tileHolder.transform.parent = roomHolder.transform;
        
    }

    private void NewObjectHolder()
    {
        objectHolder = new GameObject();
        objectHolder.name = "ObjectHolder";
        objectHolder.transform.parent = roomHolder.transform;
    }

    public bool IsWithinRoom( Vector3 _position )
    {
        if(roomHolder != null )
        {
            if (_position.x >= roomHolder.transform.position.x &&
            _position.y >= roomHolder.transform.position.y &&
            _position.x <= (roomHolder.transform.position.x + data.mapWidth) &&
            _position.y <= (roomHolder.transform.position.y + data.mapHeight))
            {
                return true;
            }
        }
        else
        {
            Debug.LogError("Room holder doesn't exist. Is this being run in edit mode?");
        }
        
        return false;
    }

    //Returns thet tile at the specified world position.
    public GameObject GetTileAtWorldPosition( Vector3 _position )
    {
        Vector3 localPosition = _position - roomHolder.transform.position;
        int index = ToIndex(localPosition);

        if (index >= 0 && index < tiles.Length)
            return tiles[index];

        return null;
    }

    //Returns an object at the specified world position.
    public GameObject GetObjectsAtWorldPosition( Vector3 _position, List<GameObject> _gameObjects )
    {
        foreach( Transform obj in objectHolder.transform )
        {
            Vector3 diff = (obj.transform.position - _position);
            diff.z = 0.0f;
            if (diff.sqrMagnitude < 0.5f )
            {
                _gameObjects.Add( obj.gameObject );
            }
        }

        return null;
    }

    //Will return the named object specified by the given string.
    //Or null I guess.
    public GameObject GetNamedObject( string _name )
    {
        ExtraData ed = null;
        foreach( Transform trans in objectHolder.transform )
        {
            ed = trans.GetComponent<ExtraData>();
            if (ed != null && ed.rawData.objectTag == _name)
                return trans.gameObject;
        }
        return null;
    }

    public string Name()
    {
        return roomHolder.name;
    }

    //Loads all the room connections into "connections."
    public void LoadConnections()
    {
        RoomConnection rc   = null;
        ExtraData ed        = null;
        foreach( Transform obj in objectHolder.transform )
        {
            rc = obj.GetComponent<RoomConnection>();
            if( rc != null )
            {
                ed = obj.GetComponent<ExtraData>();
                if( ed != null )
                {
                    //Add this to the connections list.
                    //Debug.Log("Found connection: " + ed.rawData.objectTag);
                    connections.Add(ed);
                }
                else
                {
                    Debug.LogError("Room Connections should ALWAYS have extra data. This one doesn't.");
                }
            }
        }
    }

    //Connects two rooms. The secondary room 
    public static void ConnectRooms( Room _primaryRoom, Room _secondaryRoom, string _connector1, string _connector2)
    {
        if( _primaryRoom != null && _secondaryRoom != null )
        {
            Vector3 connectionDirection = Vector3.zero;
            GameObject roomConnection1 = _primaryRoom.GetNamedObject(_connector1);
            GameObject roomConnection2 = _secondaryRoom.GetNamedObject(_connector2);
            
            int connectorIndex = _primaryRoom.WorldPosToIndex(roomConnection1.transform.position);


            int roomWidth = _primaryRoom.data.mapWidth;
            int roomHeight = _primaryRoom.data.mapHeight;

            //Slightly better block of code that uses the object's id to determine where it is in the room.
            //Its ID is an indexed location starting from 0 (bottom left) to roomWidth*roomHeight-1 (top-right).
            if( connectorIndex != 0 )
            {
                if( connectorIndex < roomWidth) //This is bottom row.
                {
                    connectionDirection.y -= 1.0f;
                }
                else if( connectorIndex > ((roomWidth * roomHeight) - roomWidth) ) //Doesn't take into account top-left. This is top row.
                {
                    connectionDirection.y += 1.0f;
                }
                else if( connectorIndex % roomWidth == 0 ) //Left edge.
                {
                    connectionDirection.x -= 1.0f;
                }
                else if( connectorIndex % roomWidth == (roomWidth-1) ) //Right edge.
                {
                    connectionDirection.x += 1.0f;
                }
                else
                {
                    Debug.LogError("Can't figure out what this connector connects to");
                }
            }
            else
            {
                Debug.LogError("Connector cannot have id 0.");
            }

            //This will move the second room to connect with the first room. Awesome!
            _secondaryRoom.roomHolder.transform.position = roomConnection1.transform.position + connectionDirection - roomConnection2.transform.position;
        }
    }

    public Vector3 GetRoomCenter()
    {
        Vector3 center = roomHolder.transform.position;
        center.x += (float)(data.mapWidth) * 0.5f;
        center.y += (float)(data.mapHeight) * 0.5f;
        return center;
    }

    public Rect GetRoomRect()
    {
        Rect returnRect = new Rect();

        returnRect.xMin = roomHolder.transform.position.x;
        returnRect.yMin = roomHolder.transform.position.y;
        
        returnRect.xMax = roomHolder.transform.position.x + data.mapWidth;
        returnRect.yMax = roomHolder.transform.position.y + data.mapHeight;

        return returnRect;
    }

    //Restart the room. Ignores any save data.
    public void RestartRoom()
    {
        DestroyObjects();
        NewObjectHolder();

        Vector3 roomPos = roomHolder.transform.position;
        roomHolder.transform.position = Vector3.zero;
        SaveLoader.LoadObjects(data, objectHolder.transform, false);
        roomHolder.transform.position = roomPos;
    }

    //Destroy the entire room.
    public void DestroyRoom()
    {
        DestroyObjects();
        DestroyTiles();
        GameObject.DestroyImmediate(roomHolder);
    }

    public void DestroyTiles()
    {
        foreach (Transform child in tileHolder.transform)
        {
            GameObject.DestroyImmediate(child.gameObject);
        }
        GameObject.DestroyImmediate(tileHolder);
    }

    public void DestroyObjects()
    {
        foreach( Transform child in objectHolder.transform )
        {
            GameObject.DestroyImmediate(child.gameObject);
        }
        GameObject.DestroyImmediate(objectHolder);
    }
}