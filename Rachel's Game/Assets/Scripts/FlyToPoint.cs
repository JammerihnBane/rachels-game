﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyToPoint : MonoBehaviour {

    public GameObject destroySound;

    public string objectName = "" ;

    public Vector3 randomBumpMin;
    public Vector3 randomBumpMax;
    public float springStrength;
    public bool isScreenPoint = true;
    
    private Vector3 velocity;

    public GameObject tracking;    

	// Use this for initialization
	void Start ()
    {
        if(objectName != null && objectName != "")
            tracking = GameObject.Find(objectName);

        velocity = new Vector3(Random.Range(randomBumpMin.x, randomBumpMax.x), Random.Range(randomBumpMin.y, randomBumpMax.y), 0f);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 trackingPos = tracking.transform.position;
        if(isScreenPoint == true )
            trackingPos = Camera.main.ScreenToWorldPoint(tracking.transform.position);

        Vector3 springAmount = trackingPos - transform.position;

        //velocity *= friction;

        //transform.position += velocity * Time.deltaTime;

        if( springAmount.sqrMagnitude < 64f )
        {
            springAmount.Normalize();
            springAmount *= 8f;
        }

        velocity *= 0.95f;
        if (velocity.sqrMagnitude <= 1f)
            velocity = Vector3.zero;
        transform.position += velocity * Time.deltaTime;
        transform.position += springAmount * springStrength * Time.deltaTime;

        if ( springAmount.magnitude < 16f )
        {
            GameObject obj = GameObject.Instantiate(destroySound);
            obj.transform.position = transform.position;

            if (tracking.GetComponent<Animator>() != null)
            {
                tracking.GetComponent<Animator>().SetTrigger("trigger");
            }

            Destroy(gameObject);
        }
    }
}
