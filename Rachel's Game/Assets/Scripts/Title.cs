﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour {

    public Text[] fadeText;

    public float fadeRate = 0.33f;
    public float targetAlpha = 1f;
    public float forcedWait = 0.5f;
    public float audioFadeRate = 0.18f;

    public bool startFading = false;

    public GameObject openingCutscene;

    private AudioSource thisSource;

    // Use this for initialization
    void Start () {
        thisSource = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        if (startFading == false && (Input.touchCount > 0 || Input.GetKey(KeyCode.Space)))
        {
            startFading = true;
        }

        if( startFading )
        {
            if (thisSource.volume > 0f)
                thisSource.volume -= Time.deltaTime * audioFadeRate;

            if (targetAlpha > 0f)
            {
                targetAlpha -= Time.deltaTime * fadeRate;
                foreach( Text txtObj in fadeText)
                {
                    Color col = txtObj.color;
                    col.a = targetAlpha;
                    txtObj.color = col;
                }
            }
            else
            {
                if(forcedWait > 0f)
                {
                    forcedWait -= Time.deltaTime;
                }
                else
                {
                    //Start the game already!
                    GameObject.Instantiate(openingCutscene);
                    Destroy(gameObject);
                }
            }
        }
	}
}
