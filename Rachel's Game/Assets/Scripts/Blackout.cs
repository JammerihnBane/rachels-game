﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Blackout : MonoBehaviour {

    public MeshRenderer mr;
    public float startingAlpha;
    public float fadeRate = 1f;

    private float targetAlpha;

	// Use this for initialization
	void Start ()
    {
        targetAlpha = startingAlpha;
        mr = GetComponent<MeshRenderer>();
        Color col = mr.material.color;
        col.a = targetAlpha;
        mr.material.color = col;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Color col = mr.material.color;
        
        if( col.a < targetAlpha )
        {
            col.a += fadeRate * Time.deltaTime;
            col.a = Mathf.Min(targetAlpha, col.a);
        }
        else if( col.a > targetAlpha )
        {
            col.a -= fadeRate * Time.deltaTime;
            col.a = Mathf.Max(targetAlpha, col.a);
        }

        mr.material.color = col;
    }

    public void FadeTo( float _alpha )
    {
        targetAlpha = _alpha;
    }
}
