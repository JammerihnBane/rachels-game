﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatBiscuit : MonoBehaviour {

    public GameObject spawnThis;
    public GameObject pickupClip;

    public static int numBiscuits = 0;
    public static int score = 0;

	// Use this for initialization
	void Start () {
        ++numBiscuits;
        GameManager.instance.SetMaxScore(numBiscuits);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Collided.");

        GameObject obj = GameObject.Instantiate(spawnThis);
        obj.transform.position = transform.position;

        GameObject sound = GameObject.Instantiate(pickupClip);
        sound.transform.position = transform.position;

        ++score;
        GameManager.instance.SetScore(score);

        Destroy(gameObject);
    }
}
