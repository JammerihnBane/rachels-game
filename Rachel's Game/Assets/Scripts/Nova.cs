﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nova : MonoBehaviour {

    public float minTwitchCooldown;
    public float randomTwitchTime;

    private float twitchTimer = 0f;
    private Animator anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        ResetTwitch();
    }
	
	// Update is called once per frame
	void Update () {
        twitchTimer -= Time.deltaTime;
        if( twitchTimer <= 0f )
            ResetTwitch();
    }

    public void ResetTwitch()
    {
        twitchTimer = Random.Range(minTwitchCooldown, minTwitchCooldown + randomTwitchTime);
        anim.SetTrigger("Twitch");
    }
}
