﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour {

    Cutscene cutscene = null;
    public bool triggered = false;

    public void Start()
    {
        cutscene = GetComponent<Cutscene>();
    }

    public void OnTriggerEnter2D()
    {
        if(triggered == false )
        {
            triggered = true;
            if (cutscene)
                cutscene.DoAction();
        }
    }
}
