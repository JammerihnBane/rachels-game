﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonDownActivator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public delegate void ButtonDelegate();

    public ButtonDelegate callback;
    public bool pointerDown;

    public void Update()
    {
        if (pointerDown)
            callback();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
    }
}