﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ECutsceneAction
{
    FADE_BLACKOUT,
    SPAWN_OBJECT,           //Object must be in the resources folder.
    WAIT,
    CONVERSATION,
    DESTROY_OBJECT,
    SHOW_UI,
    HIDE_UI,
    DISABLE_CONTROLS,
    ENABLE_CONTROLS,
    CHANGE_CAMERA_TARGET,
    TRIGGER_ANIMATION,
    SHOW_CATHEAD,
    WAIT_UNTIL_CALLED,
    CHANGE_CAMERA_SPRING,
    FADE_MUSIC
};

[System.Serializable]
public class CutsceneAction
{
    public ECutsceneAction type;
    public string parameter;
}

public class Cutscene : MonoBehaviour
{
    public List<CutsceneAction> actions;
    public bool doActionOnWake = true;
    private float waitTime = 0f;

    // Use this for initialization
    void Start ()
    {
        if(doActionOnWake == true )
            DoAction();
    }

    void Update()
    {
        if(waitTime > 0f)
        {
            waitTime -= Time.deltaTime;
            if( waitTime <= 0f )
            {
                DoAction();
            }
        }
    }

    //Does a thing, then immidiately does the next thing.
    public void DoAction()
    {
        if( actions.Count > 0 )
        {
            CutsceneAction action = actions[0];
            bool immidiateAction = true;
            switch (action.type)
            {
                case ECutsceneAction.DESTROY_OBJECT:
                    {
                        Destroy(GameObject.Find(action.parameter));
                        break;
                    }
                case ECutsceneAction.SPAWN_OBJECT:
                    {
                        GameObject obj = Resources.Load<GameObject>(action.parameter);
                        GameObject.Instantiate(obj);
                        break;
                    }
                case ECutsceneAction.FADE_BLACKOUT:
                    {
                        GameManager.instance.blackout.FadeTo(float.Parse(action.parameter));
                        break;
                    }
                case ECutsceneAction.WAIT:
                    {
                        waitTime = float.Parse(action.parameter);
                        immidiateAction = false;
                        break;
                    }
                case ECutsceneAction.CONVERSATION:
                    {
                        Conversation convo = Resources.Load<Conversation>(action.parameter);
                        if (convo != null)
                        {
                            GameObject obj = GameObject.Instantiate(GameManager.instance.textBoxPrefab, GameManager.instance.canvas);
                            TextBox tb = obj.GetComponent<TextBox>();

                            tb.conversation = convo;
                            tb.motherCutscene = this;
                        }
                        else
                        {
                            Debug.LogError("Couldn't load conversation: " + action.parameter);
                        }

                        immidiateAction = false;
                        break;
                    }
                case ECutsceneAction.HIDE_UI:
                    {
                        GameManager.instance.ShowUI(false);
                        break;
                    }
                case ECutsceneAction.SHOW_UI:
                    {
                        GameManager.instance.ShowUI();
                        break;
                    }
                case ECutsceneAction.DISABLE_CONTROLS:
                    {
                        CharacterControls.SetControl(false);
                        break;
                    }
                case ECutsceneAction.ENABLE_CONTROLS:
                    {
                        CharacterControls.SetControl(true);
                        break;
                    }
                case ECutsceneAction.CHANGE_CAMERA_TARGET:
                    {
                        Camera.main.GetComponent<TrackObject>().trackThis = GameObject.Find(action.parameter);
                        break;
                    }
                case ECutsceneAction.TRIGGER_ANIMATION:
                    {
                        string[] str = action.parameter.Split(':');
                        if (str.Length != 2)
                            Debug.LogError("Trigger animation array is the wrong size.");
                        GameObject.Find(str[0]).GetComponent<Animator>().SetTrigger(str[1]);
                        break;
                    }
                case ECutsceneAction.SHOW_CATHEAD:
                    {
                        GameManager.instance.catHead.SetActive(true);
                        break;
                    }
                case ECutsceneAction.WAIT_UNTIL_CALLED:
                    {
                        immidiateAction = false;
                        break;
                    }
                case ECutsceneAction.CHANGE_CAMERA_SPRING:
                    {
                        float cameraSpring = float.Parse(action.parameter);
                        Camera.main.GetComponent<TrackObject>().springAmount = cameraSpring;
                        break;
                    }
                case ECutsceneAction.FADE_MUSIC:
                    {
                        GameObject.Find("Music").GetComponent<Music>().FadeTo(float.Parse(action.parameter));
                        break;
                    }
            }

            //Do the next action if we can.
            actions.RemoveAt(0);
            if (immidiateAction && actions.Count > 0)
                DoAction();
        }
    }
}
