﻿using UnityEngine;
using System.Collections;

public class Utilities : MonoBehaviour {

    //Clears all children from a gameobject.
    //Does not kill the gameobject itself.
    static public void ClearAllFromThis(GameObject _obj)
    {
        //Destroy all children's children.
        foreach (Transform trans in _obj.transform)
        {
            if (trans.childCount > 0)
            {
                ClearAllFromThis(trans.gameObject);
            }
        }

        //Destroy all children.
        foreach (Transform trans in _obj.transform)
        {
            GameObject.Destroy(trans.gameObject);
        }
    }
}
