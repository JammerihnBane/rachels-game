﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class OneShotAudio : MonoBehaviour {

    public AudioSource thisSource;

    [Range(0f, 0.5f)]
    public float randomisePitch;

	// Use this for initialization
	void Start () {
        thisSource = GetComponent<AudioSource>();
        thisSource.pitch += Random.Range(-randomisePitch, randomisePitch);
    }
	
	// Update is called once per frame
	void Update () {
        if(thisSource.isPlaying == false )
        {
            Destroy(gameObject);
        }
	}
}
