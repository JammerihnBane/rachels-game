﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conversation : ScriptableObject
{
    public List<ConvoLine> lines;
}

[System.Serializable]
public class ConvoLine
{
    public Sprite speakerImage;
    public string speakerName;
    [TextArea]
    public string speakerText;
};


