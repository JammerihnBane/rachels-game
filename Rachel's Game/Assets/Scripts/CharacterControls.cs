﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class CharacterControls : MonoBehaviour
{
    public Vector3 horizontalMovement = new Vector3(0f, 0f, 0f);
    public static bool bControlEnabled = true;

    public GameObject jumpSound;
    public GameObject moveSound;
    
    public float stepCooldown;

    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private BoxCollider2D bc;
    private Animator anim;
    private bool jumping = false;
    private bool moving = false;
    private float floorDist = -1f;

    [SerializeField]
    private float jumpStrength;
    [SerializeField]
    private float horizontalFriction;
    [SerializeField]
    private GameObject cameraBall;

    private float stepTimer;

    private bool moveLeft = false;
    private bool moveRight = false;

    //Sets the player's control.
    public static void SetControl( bool _b )
    {
        bControlEnabled = _b;
        if (bControlEnabled == false)
        {
            GameObject leftButton = GameObject.Find("LeftButton");
            if (leftButton)
                leftButton.GetComponent<ButtonDownActivator>().pointerDown = false;

            GameObject rightButton = GameObject.Find("RightButton");
            if( rightButton )
                rightButton.GetComponent<ButtonDownActivator>().pointerDown = false;
        }
        GameManager.instance.ShowUI(bControlEnabled);
    }

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        bc = GetComponent<BoxCollider2D>();

        GameObject.Find("LeftButton").GetComponent<ButtonDownActivator>().callback = MoveLeft;
        GameObject.Find("RightButton").GetComponent<ButtonDownActivator>().callback = MoveRight;
        GameObject.Find("JumpButton").GetComponent<JumpButton>().callback = Jump;
        cameraBall = GameObject.Find("CameraBall");

        Jump();

        name = "Rachel";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            moveRight = true;
        if (Input.GetKey(KeyCode.LeftArrow))
            moveLeft = true;

        if (moveRight)
            Move(horizontalMovement);

        if (moveLeft)
            Move(-horizontalMovement);

        if (Input.GetKeyDown(KeyCode.Space))
            Jump();

        moveLeft = false;
        moveRight = false;

        Vector2 horizFriction = rb.velocity;
        horizFriction.x *= horizontalFriction;
        rb.velocity = horizFriction;

        CheckResetJump();

        anim.SetBool("moving", moving);
        anim.SetFloat("xSpeed", Mathf.Abs(rb.velocity.x));
        anim.SetBool("jumping", jumping);
        moving = false;

        ProcessCameraBall();

        stepTimer -= Time.deltaTime;
    }

    public void ProcessCameraBall()
    {
        Vector2 ballPos = new Vector2(transform.position.x, cameraBall.transform.position.y);
        if(floorDist >= 0f )
        {
            ballPos.y = transform.position.y - floorDist;
        }
        cameraBall.transform.position = ballPos;
    }

    //Resets the character's jump. Casts two rays on either side of the character.
    //As long as one hits, we're bueno.
    public void CheckResetJump()
    {
        //Only cast against ground objects
        int layerMask = 1 << 8;

        Vector2 leftSide = new Vector2(transform.position.x - bc.size.x * transform.localScale.x * 0.5f, transform.position.y);
        Vector2 rightSide = new Vector2(transform.position.x + bc.size.x * transform.localScale.x * 0.5f, transform.position.y);
        Vector2[] rayCasts = new Vector2[] { leftSide, rightSide };

        floorDist = -1f;
        foreach (Vector2 vec in rayCasts)
        {
            Vector2 end = vec;
            end.y -= 5f;

            RaycastHit2D hit = Physics2D.Linecast(vec, end, layerMask);
            Debug.DrawLine(vec, end);


            if (hit.transform != null)
            {
                floorDist = hit.distance;
                if(hit.distance < 0.1f && rb.velocity.y <= 0f)
                    jumping = false;    
            }
        }
    }

    public void Move(Vector2 _move)
    {
        if(bControlEnabled == true)
        {
            if (_move.x < 0f)
            {
                sr.flipX = true;
            }
            else
            {
                sr.flipX = false;
            }

            rb.velocity = new Vector2(_move.x, rb.velocity.y);

            moving = true;

            if(jumping == false && stepTimer <= 0f )
            {
                stepTimer = stepCooldown;
                GameObject.Instantiate(moveSound, transform);
            }
        }
    }

    //Causes the player to move left.
    public void MoveLeft()
    {
        moveLeft = true;
    }

    //Causes the player to move right.
    public void MoveRight()
    {
        moveRight = true;
    }

    //Causes the player to jump.
    public void Jump()
    {
        if (bControlEnabled == true)
        {
            if (jumping == false)
            {
                jumping = true;
                rb.velocity = new Vector2(0f, jumpStrength);
                GameObject.Instantiate(jumpSound, transform);
            }
        }
    }
}

