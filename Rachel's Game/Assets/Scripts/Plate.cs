﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour {
    

    public Sprite[] platePhases;
    public int[] biscuitThresholds;
    public GameObject[] instantiateThese;
    public GameObject phaseChangeSound;
    
    private int numBiscuits;
    private SpriteRenderer spr;

    private int lastIndex = 0;
    private Vector3 initialPosition;

    private int shake = 0;

	// Use this for initialization
	void Start ()
    {
        spr = GetComponent<SpriteRenderer>();
        initialPosition = transform.position;
    }

    public void FixedUpdate()
    {
        if( shake != 0 )
        {
            Vector3 pos = transform.position;
            pos.x = initialPosition.x + shake;
            transform.position = pos;

            shake *= -1;
            if( shake > 0 )
            {
                --shake;
            }
        }
    }

    public void AddBiscuit()
    {
        ++numBiscuits;
        int index = 0;
        for( int i = 0; i < biscuitThresholds.Length; ++i )
        {
            if (numBiscuits >= biscuitThresholds[i])
            {
                index = i;
            }
            else
                break;
        }

        if(lastIndex != index )
        {
            spr.sprite = platePhases[index];
            TriggerPhaseChange(index);
        }

        lastIndex = index;
    }

    public void TriggerPhaseChange(int _index)
    {
        //GetComponent<Animator>().SetTrigger("PhaseChange");
        shake = 3;

        GameObject.Instantiate(phaseChangeSound, transform);

        if (instantiateThese[_index] != null)
            GameObject.Instantiate(instantiateThese[_index]);
    }
}
