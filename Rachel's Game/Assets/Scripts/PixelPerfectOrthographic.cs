﻿using UnityEngine;
using System.Collections;

public class PixelPerfectOrthographic : MonoBehaviour {

    public float pixelsPerUnit;
    public float zoomLevel = 4;
    public float oldHeight = 0;

	// Use this for initialization
	void Start () {
        UpdateRez();

        RoundPos();
    }

    void LastUpdate()
    {
        if (oldHeight != Screen.height)
        {
            UpdateRez();
            oldHeight = Screen.height;
        }

        RoundPos();
    }

    void UpdateRez()
    {
        Camera thisCam = GetComponent<Camera>();
        if (thisCam)
        {
            //Debug.Log("Screen Width: " + Screen.width + " Screen Height: " + Screen.height);

            float orthoSize = ((Screen.height) / pixelsPerUnit) * 0.5f / zoomLevel;

            orthoSize = (1 / 16f) * Mathf.Floor(orthoSize * 16f);

            thisCam.orthographicSize = orthoSize;
            //Debug.Log("OrthoSize: " + thisCam.orthographicSize);
        }
    }

    //Updates the camera position. Makes sure it's on a pixel boundary.
    public void RoundPos()
    {
        float precision = 32.0f;
        float invPrecision = 1 / 32.0f;

        Vector3 roundedPos = transform.position;
        roundedPos.x = invPrecision * Mathf.Floor(precision * roundedPos.x);
        roundedPos.y = invPrecision * Mathf.Floor(precision * roundedPos.y);

        transform.position = roundedPos;
    }

    public Vector3 RoundPos(Vector3 _pos)
    {
        float precision = 32.0f;
        float invPrecision = 1 / 32.0f;

        Vector3 roundedPos = _pos;
        roundedPos.x = invPrecision * Mathf.Floor(precision * roundedPos.x);
        roundedPos.y = invPrecision * Mathf.Floor(precision * roundedPos.y);

        return roundedPos;
    }
}
