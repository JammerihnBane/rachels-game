﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBox : MonoBehaviour {

    public Conversation conversation;
    
    private string setText = "";
    public GameObject textSound;
    public float timeBetweenLetters = 0.01f;
    public float soundCooldown = 0.1f;

    public Text speaker;
    public Text dialogue;
    public Image speakerImage;
    public GameObject nextBoxObj;

    [HideInInspector]
    public Cutscene motherCutscene;

    private string      currentText = "";
    private int         stringIndex = -1;
    private int         lineNumber = 0;
    
    private float       timer = 0.0f;
    private float       currentSoundCooldown;

	// Use this for initialization
	void Start ()
    {
        SetText("");
        if(conversation != null)
        {
            NextLine();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (stringIndex < setText.Length )
        {
            timer += Time.deltaTime;
            currentSoundCooldown -= Time.deltaTime;
            while ( timer >= timeBetweenLetters )
            {
                timer -= timeBetweenLetters;
                ++stringIndex;
                if (currentSoundCooldown <= 0f)
                {
                    currentSoundCooldown = soundCooldown;
                    if (textSound != null)
                        Instantiate(textSound);
                }

                currentText = setText.ToUpper();
                if(stringIndex < currentText.Length )
                {
                    currentText = currentText.Insert(stringIndex, "<color=#00000000>");
                    currentText = currentText + "</color>";
                }
                dialogue.text = currentText;
                nextBoxObj.SetActive(false);
            }

            if(Input.GetKeyDown(KeyCode.Space) )
            {
                stringIndex = setText.Length - 1;
            }
        }
        else
        {
            nextBoxObj.SetActive(true);
            if (Input.touchCount > 0 || Input.GetKeyDown(KeyCode.Space))
            {
                NextLine();
            }
        }
    }

    public void SetText( string _text )
    {
        setText = _text;
        currentText = "";
        stringIndex = 0;
        timer = 0.0f;

        setText = setText.Replace("[ENVIRONMENT]", "Environment");
    }

    private void NextLine()
    {
        if( conversation != null )
        {
            if( lineNumber < conversation.lines.Count )
            {
                ConvoLine line = conversation.lines[lineNumber];
                SetText(line.speakerText);
                speaker.text = line.speakerName + ":";
                speakerImage.sprite = line.speakerImage;
            }
            else
            {
                //End the conversation.
                Destroy(gameObject);
                if (motherCutscene)
                    motherCutscene.DoAction();
            }
        }
        ++lineNumber;
    }
}
