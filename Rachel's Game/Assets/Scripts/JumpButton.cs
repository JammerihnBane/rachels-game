﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JumpButton : MonoBehaviour, IPointerDownHandler {

    public ButtonDownActivator.ButtonDelegate callback;
	
    public void OnPointerDown(PointerEventData eventData)
    {
        callback();
    }
}
