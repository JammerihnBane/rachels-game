﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkZone : MonoBehaviour {

    public ELightState changeOnEnter = ELightState.LIGHTS_FLICKER;
    public ELightState changeOnExit = ELightState.LIGHTS_ON;

    // Use this for initialization
    void Start () {
		
	}
	
    public void OnTriggerEnter2D()
    {
        Debug.Log("Trigger");
        GameManager.instance.ChangeLightState(changeOnEnter);
    }

    public void OnTriggerExit2D()
    {
        Debug.Log("Trigger");
        GameManager.instance.ChangeLightState(changeOnExit);
    }

    public void OnCollisionEnter2D()
    {
        Debug.Log("Collision");
        GameManager.instance.ChangeLightState(changeOnEnter);
    }

    public void OnCollisionExit2D()
    {
        Debug.Log("Collision");
        GameManager.instance.ChangeLightState(changeOnExit);
    }
}
