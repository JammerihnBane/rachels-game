﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackObject : MonoBehaviour {

    public GameObject trackThis;

    public Vector3 offset;

    public float springAmount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(trackThis != null)
        {
            Vector3 finalPos = transform.position;
            Vector3 trackPos = trackThis.transform.position + offset;

            Vector3 velocity = (trackPos - finalPos) * springAmount;
            velocity.z = 0f;

            finalPos += velocity * Time.deltaTime;

            transform.position = finalPos;
        }
	}
}
