﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ELightState
{
    LIGHTS_ON,
    LIGHTS_FLICKER,
    LIGHTS_OFF,
};

public class GameManager : MonoBehaviour {

    public Blackout blackout;
    public GameObject textBoxPrefab;
    public Transform canvas;

    public GameObject collectCandleSound;
    public GameObject uiHolder;
    public GameObject catHead;
    public static GameManager instance = null;

    [Range(0.1f, 10f)]
    public float flickerInterval;
    [Range(0.1f, 1.0f)]
    public float lightsOffAlpha = 0.9f;

    private ELightState lightState = ELightState.LIGHTS_ON;
    private bool flickerState = false;
    private float currentFlickerTime = 0f;

    public List<Animator> lights = new List<Animator>();

    public GameObject[] candles;
    public GameObject endCutscene;

    public Text[] score; //Array of text to set for the user's score.
    public Text[] maxScore; //Array of text to set for the user's max score.
    [HideInInspector]
    public int scoreInt = 0;
    public int maxScoreAchieved = 0;

    public Transform[] candlePoints;
    private int numCallsToCandlesPoint = 0;

    bool allCollected = false;

    // Use this for initialization
    void Awake () {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        SetScore(0);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        foreach (GameObject obj in candles)
            obj.SetActive(false);
	}

    //Return a candle point for a float candle to move to.
    public Transform GetCandlePoint()
    {
        Transform candlePoint = candlePoints[numCallsToCandlesPoint];
        
        ++numCallsToCandlesPoint;
        if (numCallsToCandlesPoint == candlePoints.Length)
            numCallsToCandlesPoint = 0;

        return candlePoint;
    }

    //Collects one of the candles.
    public void CollectCandle(int _i)
    {
        if(allCollected == false )
        {
            GameObject.Instantiate(collectCandleSound);

            candles[_i].GetComponent<Image>().color = Color.white;

            allCollected = true;
            foreach (GameObject obj in candles)
            {
                obj.SetActive(true);
                if (obj.GetComponent<Image>().color != Color.white)
                {
                    allCollected = false;
                }
            }

            if (allCollected == true)
            {
                GameObject.Instantiate(endCutscene);
                GameObject noCandles = GameObject.Find("KitchenCutsceneNoCandles");
                if (noCandles)
                    Destroy(noCandles);
            }
        }
        
    }

    public void Update()
    {
        if( lightState == ELightState.LIGHTS_FLICKER )
        {
            currentFlickerTime += Time.deltaTime;
            if( currentFlickerTime >= flickerInterval )
            {
                currentFlickerTime = 0f;

                flickerState = !flickerState;
                if (flickerState == true)
                    SetLights(lightsOffAlpha);
                else
                    SetLights(0f);
            }
        }

#if UNITY_EDITOR
        if( Input.GetKey(KeyCode.E) )
        {
            GameObject.Find("Rachel").transform.position = new Vector3(40f, 689f, -5f);
            SetScore(99);
        }
        if( Input.GetKey(KeyCode.F))
        {
            CollectCandle(0);
        }
        if (Input.GetKey(KeyCode.G))
        {
            CollectCandle(1);
        }
#endif
    }

    public void ShowUI( bool _bShow = true )
    {
        uiHolder.SetActive(_bShow);
        catHead.SetActive(_bShow);
    }

    public void SetLights( float _alpha )
    {
        bool on = true;
        if (_alpha > 0f)
            on = false;

        foreach (Animator anim in lights)
            anim.SetBool("lightOn", on);

        blackout.FadeTo(_alpha);
    }

    public void ChangeLightState( ELightState _lightState )
    {
        lightState = _lightState;
        switch (lightState)
        {
            case ELightState.LIGHTS_ON:
                {
                    SetLights(0f);
                    break;
                }
            case ELightState.LIGHTS_OFF:
                {
                    SetLights(lightsOffAlpha);
                    break;
                }
            case ELightState.LIGHTS_FLICKER:
                {
                    flickerState = true;
                    currentFlickerTime = 0f;
                    SetLights(lightsOffAlpha);
                    break;
                }
        }
    }

    public void SetScore(int _score)
    {
        foreach (Text txt in score)
            txt.text = _score.ToString();
        scoreInt = _score;

        if (scoreInt > maxScoreAchieved)
            maxScoreAchieved = scoreInt;
    }

    public void SetMaxScore(int _maxScore)
    {
        foreach (Text txt in maxScore)
            txt.text = "/" + _maxScore.ToString();
    }
}
