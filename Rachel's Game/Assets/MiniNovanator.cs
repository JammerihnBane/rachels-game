﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniNovanator : MonoBehaviour {

    public GameObject laserShot;
    public float gap;
    public float randomGap;

    public Transform hotspot;

    private float timer = 0f;

	// Use this for initialization
	void Start () {
        timer = Random.Range(gap, gap + randomGap);
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if( timer <= 0f )
        {
            Shoot();
            timer = Random.Range(gap, gap + randomGap);
        }

    }

    public void Shoot()
    {
        GameObject obj = GameObject.Instantiate(laserShot);
        obj.transform.position = hotspot.position;
    }
}

