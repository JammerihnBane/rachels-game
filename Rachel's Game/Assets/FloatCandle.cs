﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatCandle : MonoBehaviour {

    public Transform candleDestinationPoint;
    public float candleSpeed;
    public Vector3 offset;

	// Use this for initialization
	void Start ()
    {
        transform.position = GameObject.Find("Rachel").transform.position + offset;
        candleDestinationPoint = GameManager.instance.GetCandlePoint();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 difference = candleDestinationPoint.position - transform.position;
        if (difference.sqrMagnitude > 4)
        {
            difference.Normalize();
            difference *= candleSpeed;

            transform.position += difference * Time.deltaTime;
        }
        else
        {
            transform.position = candleDestinationPoint.position;
        }
	}
}
