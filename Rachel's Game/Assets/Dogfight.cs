﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dogfight : MonoBehaviour {

    public GameObject nova;
    public GameObject ufo;

    public int numUFOs;

    [Range(0f, 5f)]
    public float gapMin;
    [Range(0f, 5f)]
    public float gapMax;
    public float novaGap;
    public float positionRandomness;

    private float spawnThingTimer = 0f;

	// Use this for initialization
	void Start () {
        SpawnThing();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if( spawnThingTimer > 0f )
        {
            spawnThingTimer -= Time.deltaTime;
            if( spawnThingTimer <= 0f )
            {
                SpawnThing();
            }
        }
	}

    public void SpawnThing()
    {
        if( numUFOs > 0 )
        {
            --numUFOs;
            GameObject obj = GameObject.Instantiate(ufo);
            spawnThingTimer = Random.Range(gapMin, gapMax);

            obj.transform.position = transform.position;
            obj.transform.position += new Vector3(Random.Range(-positionRandomness, positionRandomness), Random.Range(-positionRandomness * 0.5f, positionRandomness*0.5f), 0f);

            if (numUFOs == 0)
            {
                spawnThingTimer = novaGap;
            }
        }
        else
        {
            GameObject obj = GameObject.Instantiate(nova);
            obj.transform.position = transform.position;
            spawnThingTimer = 0f;
        }
    }
}
