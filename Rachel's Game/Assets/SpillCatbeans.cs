﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpillCatbeans : MonoBehaviour {

    public float spillSpringStrength = 1.0f;
    public float spillInterval = 0.1f;
    public GameObject flyingCatBiscuitPrefab;
    public GameObject ping;
    public Vector3 randomBumpMin;
    public Vector3 randomBumpMax;

    private float currentSpill = 0.01f;
    private static GameObject catHead = null;
    private static Animator catHeadAnimator = null;
    private static Vector3 startingPoint = Vector3.zero;
    private static Plate plate = null;
    private static Cutscene myCutscene = null;

    private int scoreRemaining;

	// Use this for initialization
	void Start () {
        if( catHead == null )
        {
            catHead = GameObject.Find("CatHead");
            plate = GameObject.Find("Plate").GetComponent<Plate>();
            myCutscene = GameObject.Find("EndCutscene(Clone)").GetComponent<Cutscene>();
            catHeadAnimator = catHead.GetComponent<Animator>();
            startingPoint = Camera.main.ScreenToWorldPoint(catHead.transform.position);
        }

        scoreRemaining = GameManager.instance.scoreInt;

        transform.position = startingPoint;
    }
	
	// Update is called once per frame
	void Update () {
		if(currentSpill > 0f)
        {
            currentSpill -= Time.deltaTime;
            if( currentSpill <= 0f )
            {
                SpillBean();
            }
        }
	}

    public void SpillBean()
    {
        plate.AddBiscuit();

        GameObject obj = GameObject.Instantiate(flyingCatBiscuitPrefab);
        FlyToPoint ftp = obj.GetComponent<FlyToPoint>();
        obj.transform.position = startingPoint;
        ftp.objectName = "";
        ftp.tracking = plate.gameObject;
        ftp.isScreenPoint = false;
        ftp.springStrength = spillSpringStrength;
        ftp.randomBumpMin = randomBumpMin;
        ftp.randomBumpMax = randomBumpMax;
        catHeadAnimator.SetTrigger("trigger");
        GameObject.Instantiate(ping);

        --scoreRemaining;
        GameManager.instance.SetScore(scoreRemaining);
        if (scoreRemaining > 0)
            currentSpill = spillInterval;
        else
            myCutscene.DoAction();
    }
}
